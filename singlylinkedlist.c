#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int number;
	struct node *next ;
}node;

typedef struct llist{
	node * head;
}llist;

node * create (int data){
	node * head;
	head = (node *)(malloc(sizeof(node *)));
	head -> number = data;
	head -> next = NULL;
	return head;
}

llist * insert_beg(llist * ll, node * a){
	a -> next = ll -> head;
	ll -> head = a;
	return ll;
}

void insert_end(llist * ll, node * a){
	node * temp = ll -> head;
	while ((temp -> next) != NULL)
		temp = temp -> next;
	temp -> next = a;
}

void insert_after(llist * ll, int pos, node * a){
	node * temp = ll -> head;
	for(int i = 1; i<pos; i++)
		temp = temp -> next;
	node * temp1 = (node *) malloc(sizeof(node));
	temp1 = temp -> next;
	temp -> next = a;
	a -> next = temp1;
}

void print_list(llist * ll){
	node * temp = ll -> head;
	while(temp != NULL){
		printf("%d\t", temp->number);
		temp = temp -> next;
	}
	printf("\n");
}

int main(){
	llist * ll = (llist *)(malloc(sizeof(llist *)));
	node * h = create(4);
	ll -> head = h;
	print_list(ll);
	node * a1 = create(3);
	node * a2 = create(5);
	node * a3 = create(6);
	ll = insert_beg(ll, a1);
	print_list(ll);
	insert_end(ll, a2);
	print_list(ll);
	insert_after(ll, 2, a3);
	print_list(ll);
	return 0;
}
