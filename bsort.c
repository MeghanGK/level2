#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void insize(int * n){
    printf("No of elements to be entered : ");
    scanf("%d", n);
}

void inarraychar(int n, char arr[n]){
    for(int i = 0; i < n; i++){
	printf("Enter character:");
    	scanf(" %c", &arr[i]);
	}
}

void inarrayint(int n, int arr[n]){
    for(int i = 0; i < n; i++)
    	scanf("%d", &arr[i]);
}

int comparint(const void *p1, const void *p2){	
	return (*(int *)p1)-(*(int *)p2);
}

int comparchar(const void *p1, const void *p2){	
	return (*(char *)p1)-(*(char *)p2);
}
void bubblesort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *)){
	void * t = malloc(size);
	int s = 1;
	void * temp;
	temp = base;
	while(s != 0){
		s = 0;
		base = temp;
		for(int i = 0; i < (nmemb - 1); i ++){
			if((*compar)(base, base+size) > 0){
				s ++;
				memcpy(t, base, size);
				memcpy(base, (base+size), size);
				memcpy((base+size), t, size);
			}
			base += size;
		}
	}
	free(t);
}
	
void outarraychar(int n, char arr[n]){
    for(int i = 0; i < n; i++)
    	printf("%c\t", arr[i]);
    printf("\n");
}

void outarrayint(int n, int arr[n]){
    for(int i = 0; i < n; i++)
    	printf("%d\t", arr[i]);
    printf("\n");
}

int main(){
	int n;
	insize(&n);
	
	char a[n];
	inarraychar(n, a);
	
	bubblesort(a, n, sizeof(char), comparchar);
	outarraychar(n, a);
	
	int b[n];
	inarrayint(n, b);
	
	bubblesort(b, n, sizeof(int), comparint);
	outarrayint(n, b);
	
	return 0; 
}