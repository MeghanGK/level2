#include <stdio.h>
#include <stdlib.h>

struct node{
	int n;
	struct node* p;
};

struct node * createll(int n){
	struct node * start = (struct node *)malloc(sizeof(struct node));
	struct node * ptr = start;

	for(int i = 0; i < (n-1); i++){
		ptr -> p = (struct node *)malloc(sizeof(struct node));
		ptr =  ptr -> p;
	}
	ptr = NULL;	
	return start;
}

void insertll(struct node* start){
	struct node * ptr = start;
	while(ptr != NULL){
		printf("Enter a number");
		scanf("%d", &ptr->n);
		ptr = ptr-> p;
	}
}

void printll(struct node* start){
	struct node * ptr = start;
	while(ptr != NULL){
		printf("%d\t", ptr -> n);
		ptr = ptr -> p;
	}
}

void deletell(struct node * start){
	struct node * ptr1 = start;
	struct node * ptr2;
	while(ptr1 != NULL){
		ptr2 = ptr1->p;
		free(ptr1);
		ptr1 = ptr2;
	}
	free(ptr1);
}

int main(){
	struct node * start = createll(5);
	insertll(start);
	printll(start);
	deletell(start);
}