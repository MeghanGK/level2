#ifndef LL
	#define LL
		extern int errno;
		
		struct node * createll();

		struct node * insertll(struct node* start, int position, int number);

		void printll(struct node* start);

		struct node * deletell(struct node * start, int position);

#endif